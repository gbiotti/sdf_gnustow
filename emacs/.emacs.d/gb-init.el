;;; gb-init.el --- Emacs tangled config -*- mode: emacs-lisp; lexical-binding: t; -*-

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ATTENZIONE: NON MODIFICARE QUESTO FILE!
;; File generato automaticamente
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Copyright (C) 2020 Geraldo Biotti

;; Compatiblity: emacs-version >= 27

;; This file is not part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Questo file viene generato automaticamente a partire dal
;; suo file sorgente scritto in org-mode usando la tecnica
;; del "literate-programming"
;; Non modificare questo file.  Ogni modifica a questo file
;; e' destinata ad essere persa e sovrascritta alla prossima
;; generazione dal file sorgente.

;;              ATTENZIONE: NON MODIFICARE QUESTO FILE!

;;; Code:

(defun gb/emacs/config-visit ()
  "Visita il file di configurazione 'literate programming'.
Da notare che il file deve essere impostato nella variabile
'gb/emacs/org-conf-filename' che deve essere definita in 'init.el'"
  (interactive)
  (find-file gb/emacs/org-conf-filename)
  )

(defun gb/emacs/config-reload ()
  "Effettual il reload del file di configurazione.
Esegue quindi nuovamente quelle operazioni sul file di configurazione
'literate programming' che sono state eseguite in 'int.el' all'avvio di Emacs.
Da notare che il file deve essere impostato nella variabile
'gb/emacs/org-conf-filename' che deve essere definita in 'init.el'
Assume che 'org' sia gia' stato caricato."
  (interactive)
  (org-babel-load-file gb/emacs/org-conf-filename)
  )

(when (eq system-type 'windows-nt)
  (cond ((find-font (font-spec :name "Cascadia Code PL"))
         (add-to-list 'default-frame-alist '(font . "Cascadia Code PL-10"))
         (set-face-attribute 'default nil :font "Cascadia Code PL-10"))
        ((find-font (font-spec :name "DejaVu Sans mono"))
         (add-to-list 'default-frame-alist '(font . "DejaVu Sans Mono-10"))
         (set-face-attribute 'default nil :font "DejaVu Sans Mono-10"))
        ((find-font (font-spec :name "Consolas"))
         (add-to-list 'default-frame-alist '(font . "Consolas-10"))
         (set-face-attribute 'default nil :font "Consolas-10"))
        ((find-font (font-spec :name "Inconsolata"))
         (add-to-list 'default-frame-alist '(font . "Inconsolata-10"))
         (set-face-attribute 'default nil :font "Inconsolata-10"))
        )
  )

(setq inhibit-startup-screen t)

(menu-bar-mode -1)

(tool-bar-mode -1)
(setq tool-bar-style 'image)

(toggle-scroll-bar -1)

(setq use-file-dialog nil)

(setq use-dialog-box nil)

(setq make-backup-files t
      auto-save-default t)

(use-package emacs
  :bind (("C-z" . nil)
         ("C-h h" . nil))
  )

;; (use-package emacs
;;  :bind ([remap list-buffers] . ibuffer)
;;  )
(global-set-key [remap list-buffers] 'ibuffer)

(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)

;; backwards compatibility as default-buffer-file-coding-system
;; is deprecated in 23.2.
(if (boundp 'buffer-file-coding-system)
    (setq-default buffer-file-coding-system 'utf-8)
  (setq default-buffer-file-coding-system 'utf-8))

;; Treat clipboard input as UTF-8 string first; compound text next, etc.
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;; Messages encoding system
(setq locale-coding-system 'utf-8)

(when (window-system)
  (global-hl-line-mode 1))

(setq-default truncate-lines t)

(show-paren-mode 1)

(setq scroll-conservatively most-positive-fixnum)

(setq hscroll-step 1)

(setq visible-bell t
      ring-bell-function 'ignore)

(delete-selection-mode t)

(setq-default indent-tabs-mode nil)

(setq-default tab-width 4)

(setq tab-stop-list 
      (number-sequence 4 200 4))

;; Devo caricare il modulo di supporto per la stgampa
(require 'ps-print)
;; Imposto il formato pagina a "A4"
(setq ps-paper-type 'a4)
;; (setq ps-print-color-p 'black-white)
(setq doc-view-continuous t)
(cond ((eq system-type 'windows-nt)
       ;; Windows-specific code goes here.
       ;; ATTENZIONE
       ;; Se si installa una versione diversa di GhostScript RICORDARSI
       ;; di modificare qui i percorsi!!!!!
       (setq ps-lpr-command "C:/Program Files/gs/gs9.50/bin/gswin64c.exe")
       (setq ps-lpr-switches '("-q"
                               "-dNOPAUSE"
                               "-dBATCH"
                               "-dNoCancel"
                               "-sDEVICE=mswinpr2"
                               ;; "-IC:/Program Files/gs/gs9.50/lib"
                               ;; "-sFONTPATH=C:/Windows/Fonts"
                               ;; "-sOutputICCProfile=default_cmyk.icc"
                               ;; "-dBitsPerPixel=24"
                               ;; "-dEmbedAllFonts=true"
                               ))
       (setq doc-view-ghostscript-program "C:/Program Files/gs/gs9.50/bin/gswin64c.exe")
       (setq ps-printer-name t)
       (setq ps-printer-name-option nil)
       )
      ((eq system-type 'gnu/linux)
       ;; Linux-specific code goes here.
       ))

;; Start Emacs fullscreen mode
;; (add-hook 'emacs-startup-hook 'toggle-frame-maximized)

(use-package async
  :ensure t
  :init
  (dired-async-mode 1)
  )

(use-package all-the-icons
  :if (window-system)
  :ensure t
  :demand t
  )

(use-package all-the-icons-dired
  :if (window-system)
  :ensure t
  :defer t
  :after all-the-icons
  :hook (dired-mode-hook . all-the-icons-dired-mode)
  )

(use-package all-the-icons-ibuffer
  :if (window-system)
  :ensure t
  :defer t
  :hook (ibuffer-mode-hook . all-the-icons-ibuffer-mode)
  :after all-the-icons
  )

(use-package doom-themes
  :if (window-system)
  :ensure t
  :defer t
  :config
  ;; Global settings (defaults)
  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
        doom-themes-enable-italic t) ; if nil, italics is universally disabled

  ;; Enable flashing mode-line on errors
  (doom-themes-visual-bell-config)

  ;; Enable custom neotree theme (all-the-icons must be installed!)
  ;; (doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-colors") ; use the colorful treemacs theme
  (doom-themes-treemacs-config)

  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config)
  )

(use-package spacemacs-theme
  :if (window-system)         
  :ensure t
  :defer t
  )

(use-package material-theme
  :if (window-system)         
  :ensure t
  :defer t
  )

(use-package zenburn-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package monokai-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package gruvbox-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package modus-themes
  ;; Da notare che questo tema e' valido sia in ambiente grafico
  ;; che in ambiente terminale. Per questo lo carico comunque
  ;; indipendentemente dal fatto che sia in "window-system" o meno
  ;; :if (window-system)
  :ensure t
  :defer t
  )

(use-package zerodark-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package atom-one-dark-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package dracula-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package color-theme-sanityinc-tomorrow
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package solarized-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package moe-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package seti-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(use-package base16-theme
  :if (window-system)
  :ensure t
  :defer t
  )

(if (display-graphic-p)
    (progn
      ;; Add all your customizations prior to loading the themes
      (setq modus-themes-slanted-constructs t
            modus-themes-bold-constructs nil
            modus-themes-region 'no-extend)
      ;; Load the theme files before enabling a theme (else you get an error).
      (modus-themes-load-themes)
      ;; Load the theme of your choice:
      ;; OR (modus-themes-load-vivendi)
      ;; OR (modus-themes-load-operandi)
      (modus-themes-load-vivendi))
  (progn
    ;; Add all your customizations prior to loading the themes
    (setq modus-themes-slanted-constructs t
          modus-themes-bold-constructs nil
          modus-themes-region 'no-extend)
    ;; Load the theme files before enabling a theme (else you get an error).
    (modus-themes-load-themes)
    ;; Load the theme of your choice:
    ;; OR (modus-themes-load-vivendi)
    ;; OR (modus-themes-load-operandi)
    (modus-themes-load-vivendi))
  )

(use-package doom-modeline
  :if (window-system)
  :ensure t
  :after all-the-icons
  ;;:init (doom-modeline-mode 1)
  :hook (after-init-hook . doom-modeline-mode)
  )

(use-package minions
  :if (window-system)
  :ensure t
  :hook (after-init-hook . minions-mode)
  :custom (doom-modeline-minor-modes t)
  )

(setq line-number-mode   t
      column-number-mode t)

;; Per adesso non mostro i numeri di riga sul lato della window
;; (global-display-line-numbers-mode)

(use-package dashboard
  :ensure t
  :if (window-system)
  :config
  (dashboard-setup-startup-hook)
  ;; (setq dashboard-startup-banner "~/.emacs.d/img/Logog-b.png")
  (setq dashboard-startup-banner          "~/.emacs.d/img/Logog-b.png"
        ;; dashboard-startup-banner          "~/.emacs.d/img/Logob-w.png"
        dashboard-set-heading-icons       t
        dashboard-set-file-icons          t
        dashboard-image-banner-max-height 250
        dashboard-image-banner-max-width  250)
  )

(use-package which-key
  :ensure t
  :defer 5
  :delight
  :commands which-key-mode
  :config
  (which-key-mode)
  )

(use-package hydra
  :ensure t
  :defer t
  )

(use-package dashboard
  :ensure t
  :if (window-system)
  :config
  (dashboard-setup-startup-hook)
  ;; (setq dashboard-startup-banner "~/.emacs.d/img/Logog-b.png")
  (setq dashboard-startup-banner          "~/.emacs.d/img/Logog-b.png"
        ;; dashboard-startup-banner          "~/.emacs.d/img/Logob-w.png"
        dashboard-set-heading-icons       t
        dashboard-set-file-icons          t
        dashboard-image-banner-max-height 250
        dashboard-image-banner-max-width  250)
  )

(use-package ivy
  :ensure t
  :delight
  ;; :hook (after-init-hook . ivy-mode)
  :custom
  (ivy-use-virtual-buffers t)
  (enable-recursive-minibuffers t)
  (ivy-count-format "%d/%d ")
  :config
  ;; Key bingings - Ivy based interface to stanard commands
  ;; (global-set-key (kbd "C-x b") 'ivy-switch-buffer)
  (global-set-key (kbd "C-c v") 'ivy-push-view)
  (global-set-key (kbd "C-c V") 'ivy-pop-view)
  ;; Key bindints - Ivy resume
  (global-set-key (kbd "C-c C-r") 'ivy-resume)
  (ivy-mode 1)
  )

(use-package swiper
  :ensure t
  :after ivy
  :config
  ;; Key bindings - Ivy based interface to standard commands
  (global-set-key (kbd "C-s") 'swiper-isearch)
  )

(use-package counsel
  :ensure t
  :delight
  :after (ivy swiper)
  :config
    (counsel-mode t)
    ;; (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
    ;; (global-set-key (kbd "C-c g") 'counsel-git)
    ;; (global-set-key (kbd "C-c j") 'counsel-git-grep)
    ;; ;; (global-set-key (kbd "C-c k") 'counsel-ag)
    ;; ;; (global-set-key (kbd "C-x l") 'counsel-locate)
    ;; ;; (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
    ;; Key bindings - Ivy/Counsel interface to standard commands
    (global-set-key (kbd "M-x") 'counsel-M-x)
    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    (global-set-key (kbd "M-y") 'counsel-yank-pop)
    (global-set-key (kbd "<f1> f") 'counsel-describe-function)
    (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
    (global-set-key (kbd "<f1> l") 'counsel-find-library)
    (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
    (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
    (global-set-key (kbd "<f2> j") 'counsel-set-variable)
    (global-set-key (kbd "C-x b") 'counsel-switch-buffer)
    ;; Key bindings - Ivy/Counsel interface to shell and system tools
    (global-set-key (kbd "C-c c") 'counsel-compile)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c j") 'counsel-git-grep)
    (global-set-key (kbd "C-c L") 'counsel-git-log)
    (global-set-key (kbd "C-c k") 'counsel-rg)
    (global-set-key (kbd "C-c m") 'counsel-linux-app)
    (global-set-key (kbd "C-c n") 'counsel-fzf)
    (global-set-key (kbd "C-x l") 'counsel-locate)
    (global-set-key (kbd "C-c J") 'counsel-file-jump)
    (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
    (global-set-key (kbd "C-c w") 'counsel-wmctrl)
    ;; Key bindings - Counsel other commands
    (global-set-key (kbd "C-c b") 'counsel-bookmark)
    (global-set-key (kbd "C-c d") 'counsel-descbinds)
    (global-set-key (kbd "C-c g") 'counsel-git)
    (global-set-key (kbd "C-c o") 'counsel-outline)
    (global-set-key (kbd "C-c t") 'counsel-load-theme)
    (global-set-key (kbd "C-c F") 'counsel-org-file)
  )

(use-package counsel-etags
  :disabled
  :ensure t
  :after counsel
  ;; :bind (("C-]" . counsel-etags-find-tag-at-point))
  :init
  (add-hook 'prog-mode-hook
        (lambda ()
          (add-hook 'after-save-hook
                    'counsel-etags-virtual-update-tags 'append 'local)))
  :custom
  (counsel-etags-update-interval 60)
  :config
  (push "build" counsel-etags-ignore-directories)
  )

(use-package counsel-css
  :disabled
  :ensure t
  :defer t
  :after counsel
  :hook (css-mode-hook . counsel-css-imenu-setup)
  )

(use-package ivy-rich
  :ensure t
  :after (ivy counsel)
  ;; :init
  ;; (ivy-rich-mode 1)
  :config
  (ivy-rich-mode 1)
  )

(use-package all-the-icons-ivy-rich
  :if (window-system)
  :ensure t
  :after (ivy counsel ivy-rich all-the-icons)
  ;; :init
  ;; (all-the-icons-ivy-rich-mode 1)
  :config
  (all-the-icons-ivy-rich-mode 1)
  )

(use-package ivy-hydra
  :ensure t
  :defer t
  :after (ivy hydra)
  )

(use-package amx
  :ensure t
  :defer t
  :after (:all counsel)
  ;; :bind (("M-X" . amx-major-mode-commands))
  :config (amx-mode t)
  )

(use-package org
  :defer t
  :config
  ;; Aggiungo exporter normalmente non abilitati
  (add-to-list 'org-export-backends 'ascii)
  (add-to-list 'org-export-backends 'beamer)
  (add-to-list 'org-export-backends 'md)
  (add-to-list 'org-export-backends 'org)
  (progn (add-to-list 'org-latex-packages-alist '("" "tabularx" nil))
         (add-to-list 'org-latex-packages-alist '("" "tabu" nil))
         )
  )

(use-package org-bullets
  :if (window-system)
  :ensure t
  :defer t
  :hook (org-mode-hook . org-bullets-mode)
  :after (org)
  :custom 
  (org-hide-leading-stars t)
  )

(use-package org-superstar
  :disabled
  :if (window-system)
  :ensure t
  :defer t
  :after org
  :hook (org-mode-hook . org-superstar-mode)
  :custom 
  (org-hide-leading-stars t)
  )

(use-package org-edna
  :ensure t
  :defer t
  :after org
  :hook (org-mode-hook . org-edna-mode)
  :config
  (org-edna-load)
  )

(use-package htmlize
  :ensure t
  :defer t
  )

(use-package beacon
  :if (window-system)
  :ensure t
  :defer t
  :delight
  :hook (after-init-hook . beacon-mode)
  :custom
  (beacon-blink-when-focused t)
  ;;(beacon-size 64)
  :config
  (beacon-mode 1)
  )

(use-package symon
  :ensure t
  :defer t
  )

(use-package try
  :disabled
  :ensure t
  :defer t
  )

(use-package avy
  :ensure t
  )

(use-package ace-window
  :ensure t
  :defer t
  :after avy
  :bind ([remap other-window] . ace-window)
  )

(use-package company
  :ensure t
  :defer t
  :delight
  ;; :after yasnippet
  :custom
  (company-idle-delay 0.5)
  (company-mimimum-prefix-length 3)
  :hook (;;(prog-mode-hook . company-mode)
         (after-init-hook . global-company-mode))
  )

(use-package company-quickhelp
  :ensure t
  :defer t
  :after company
  :custom
  (company-quickhelp-delay 0.1)
  :config
  (company-quickhelp-mode 1)
  )

(use-package origami
  :ensure t
  :defer t
  :commands origami-mode
  )

(use-package undo-tree
  ;; Treat undo history as a tree
  :ensure t
  :defer t
  :delight "Ut"
  :bind (("C-z" . undo)
         ("C-S-z" . undo-tree-redo))
  :config
  (progn
    (global-undo-tree-mode)
    (setq undo-tree-visualizer-timestamps t)
    (setq undo-tree-visualizer-diff t))
  )

(use-package magit
  :ensure t
  :defer t
  :after (ivy)
  :bind ("C-x g" . 'magit-status)
  )

(use-package gitconfig-mode
  :ensure t
  :defer 5
  :mode ("/\\.gitconfig\\'"
         "/\\.git/config\\'"
         "/modules/.*/config\\'"
         "/git/config\\'"
         "/\\.gitmodules\\'"
         "/etc/gitconfig\\'")
  )

(use-package gitignore-mode
  :ensure t
  :defer 5
  :mode ("/\\.gitignore\\'"
         "/info/exclude\\'"
         "/git/ignore\\'")
  )

(use-package gitattributes-mode
  :ensure t
  :defer 5
  :mode ("/\\.gitattributes\\'"
         "/info/attributes\\'"
         "/git/attributes\\'")
  )

(use-package git-timemachine
  :ensure t
  :defer t
  :commands git-timemachine
  )

(use-package treemacs
  :ensure t
  :defer t
  :commands treemacs
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                 (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay      0.5
          treemacs-directory-name-transformer    #'identity
          treemacs-display-in-side-window        t
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              5000
          treemacs-file-extension-regex          treemacs-last-period-regex-value
          treemacs-file-follow-delay             0.2
          treemacs-file-name-transformer         #'identity
          treemacs-follow-after-init             t
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-move-forward-on-expand        nil
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                      'left
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    nil
          treemacs-recenter-after-tag-follow     nil
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'on-distance
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              nil
          treemacs-silent-refresh                nil
          treemacs-sorting                       'alphabetic-asc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-user-mode-line-format         nil
          treemacs-user-header-line-format       nil
          treemacs-width                         35
          treemacs-workspace-switch-cleanup      nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-magit
  :after (treemacs magit)
  :ensure t
  :defer t
  )

(use-package treemacs-projectile
  :after (treemacs projectile)
  :ensure t
  :defer t
  )

(use-package treemacs-all-the-icons
  :after (treemacs all-the-icons)
  :ensure t
  :defer t
  )

(use-package restclient
  :ensure t
  :defer t
  )

(use-package company-restclient
  :ensure t
  :after (company restclient)
  :config
  (add-to-list 'company-backends 'company-restclient)
  )

(use-package elfeed
  ;;
  :disabled
  ;;
  :ensure t
  :defer t
  ;; Imposto la directory del db di elfeed per stare dentro .emacs.d
  :custom ((elfeed-db-directory "~/Dropbox/Sync/emacs/elfeed/db")
           (elfeed-enclosure-default-dir "~/Downloads/elfeed"))
  :config
  ;; (setq elfeed-feeds
  ;;       '("http://status.register.it/history.rss"
  ;;         "https://golem.linux.it/wp/feed/"
  ;;         ("http://dilbert.com/feed" Divertimento Webcomic)
  ;;         ("https://xkcd.com/rss.xml" Divertimento Webcomic)
  ;;         ("http://www.soft-land.org/rss/softland/commenti.rss" Divertimento Soft-land)
  ;;         ("http://www.soft-land.org/rss/softland/sdsm.rss" Divertimento Soft-land)
  ;;         ("http://www.soft-land.org/rss/softland/ospiti.rss" Divertimento Soft-land)
  ;;         ("https://bufalopedia.blogspot.com/feeds/posts/default" Antibufala Attivissimo)
  ;;         ("http://feeds.feedburner.com/Disinformatico" Antibufala Attivissimo)
  ;;         ("https://undicisettembre.blogspot.com/feeds/posts/default" Antibufala Attivissimo)
  ;;         ("https://complottilunari.blogspot.com/feeds/posts/default" Antibufala Attivissimo)
  ;;         ("http://www.valigiablu.it/feed/" Antibufala)
  ;;         ("https://blog.mikrotik.com/rss/?cat=security" CyberSecurity MikroTi)
  ;;         ("https://www.cert.garr.it/certrss" CyberSecurity CERT Cert-Italiani)
  ;;         ("https://www.certnazionale.it/news/feed/" CiberSecurity CERT Cert-Italiani)
  ;;         ("https://www.commissariatodips.it/feeds/rss.xml" CiberSecurity CERT Cert-Italiani)
  ;;         ("https://www.cert-pa.it/feed/" CiberSecurity CERT Cert-Italiani)
  ;;         ("https://www.us-cert.gov/ncas/all.xml" CiberSecurity CERT Cert-USA)
  ;;         ("https://www.us-cert.gov/ncas/alerts.xml" CiberSecurity CERT Cert-USA)
  ;;         ("https://www.us-cert.gov/ncas/bulletins.xml" CiberSecurity CERT Cert-USA)
  ;;         ("https://www.us-cert.gov/ncas/tips.xml" CiberSecurity CERT Cert-USA)
  ;;         ("https://www.us-cert.gov/ncas/current-activity.xml" CiberSecurity CERT Cert-USA)
  ;;         ("https://www.microsoft.com/technet/security/bulletin/secrss.aspx" CiberSecurity CERT Cert-USA Cert-Microsoft)
  ;;         ("https://www.microsoft.com/technet/security/bulletin/RssFeed.aspx?snscomprehensive" CiberSecurity CERT Cert-USA Cert-Microsoft)
  ;;         ("https://blogs.technet.microsoft.com/msrc/feed/" CiberSecurity CERT Cert-USA Cert-Microsoft)
  ;;         ("https://www.kaspersky.com/blog/feed/" CiberSecurity Kaspersky)
  ;;         ("https://securelist.com/feed/" CiberSecurity Kaspersky)
  ;;         ("https://threatpost.com/feed/" CiberSecurity Kaspersky)
  ;;         ("http://securityinfo.it/feed/?cat=251"  CiberSecurity SecurityInfo)
  ;;         ("https://protesilaos.com/news.xml" Emacs)
  ;;         ("https://protesilaos.com/codelog.xml" Emacs)
  ;;         ("http://www.virtuouscode.com/feed/" Emacs)
  ;;         ("http://www.virtuouscode.com/comments/feed/" Emacs)
  ;;         ("http://ergoemacs.org/emacs/blog.xml" Emacs)
  ;;         ("http://xahlee.info/comp/blog.xml" Emacs)
  ;;         ("http://xahlee.info/js/blog.xml" Emacs)
  ;;         ("http://xahlee.info/math/blog.xml" Emacs)
  ;;         ("https://alexschroeder.ch/wiki/feed/full/" Emacs)
  ;;         ("http://emacshorrors.com/feed.atom" Emacs)
  ;;         ("http://emacsredux.com/atom.xml" Emacs)
  ;;         ("https://emacspeak.blogspot.com/feeds/posts/default" Emacs)
  ;;         ("https://endlessparentheses.com/atom.xml" Emacs)
  ;;         ("http://www.howardism.org/index.xml" Emacs)
  ;;         ("http://irreal.org/blog/?feed=rss2" Emacs)
  ;;         ("https://www.masteringemacs.org/feed" Emacs)
  ;;         ("http://mbork.pl?action=rss" Emacs)
  ;;         ("http://emacsblog.org/feed/" Emacs)
  ;;         ("http://nullprogram.com/feed/" Emacs)
  ;;         ("https://oremacs.com/atom.xml" Emacs)
  ;;         ("http://planet.emacsen.org/atom.xml" Emacs)
  ;;         ("https://planet.emacslife.com/atom.xml" Emacs)
  ;;         ("http://pragmaticemacs.com/feed/" Emacs)
  ;;         ("https://emacs.stackexchange.com/feeds" Emacs)
  ;;         ("http://sachachua.com/blog/feed/" Emacs)
  ;;         ("https://babbagefiles.xyz/index.xml" Emacs)
  ;;         ("https://babbagefiles.blogspot.com/feeds/posts/default" Emacs)
  ;;         ("http://whattheemacsd.com/atom.xml" Emacs)
  ;;         ("https://www.wisdomandwonder.com/feed" Emacs)
  ;;         ("https://cestlaz.github.io/rss.xml" Emacs)
  ;;         ("https://bzg.fr/index.xml" Emacs)
  ;;         ("http://kitchinresearchgroup.disqus.com/latest.rss" Emacs)
  ;;         ("https://noonker.github.io/index.xml" Emacs)
  ;;         ("https://codingquark.com/feed.xml" Emacs)
  ;;         ("http://xenodium.com/rss.xml" Emacs)
  ;;         ("https://karthinks.com/blog/index.xml" Emacs)
  ;;         ("http://joshrollinswrites.com/index.xml" Emacs)
  ;;         ("https://punchagan.muse-amuse.in/feed.xml" Emacs)
  ;;         ("https://willschenk.com/feed.xml" Emacs)
  ;;         ("https://emacs.cafe/feed.xml" Emacs)
  ;;         ("https://groups.google.com/forum/feed/git-for-windows/msgs/rss.xml?num=50" Git)
  ;;         ("https://groups.google.com/forum/feed/git-users/msgs/rss.xml?num=50" Git)
  ;;         ("https://groups.google.com/forum/feed/git-packagers/topics/rss.xml?num=50" Git)
  ;;         ("https://groups.google.com/group/idempiere/feed/rss_v2_0_msgs.xml" iDempiere)
  ;;         ("https://groups.google.com/group/adempiere-colombia/feed/rss_v2_0_msgs.xml" iDempiere)
  ;;         ("https://groups.google.com/group/idempiere-dev/feed/rss_v2_0_msgs.xml" iDempiere)
  ;;         ("https://groups.google.com/group/idempiere-es/feed/rss_v2_0_msgs.xml" iDempiere)
  ;;         ("https://groups.google.com/group/idempiere-italia/feed/rss_v2_0_msgs.xml" iDempiere)
  ;;         ("https://www.liberliber.it/online/feed/" Ebooks Letteratura)
  ;;         ("https://www.paginatre.it/online/feed/" Ebooks Letteratura)
  ;;         ("http://it.feedbooks.com/books/recent.atom?lang=it" Ebooks Letteratura)
  ;;         ("http://pennablu.it/feed/" Ebooks Letteratura)
  ;;         ("https://www.reddit.com/r/bashonubuntuonwindows/.rss" Microsoft WSL)
  ;;         ("https://blogs.msdn.microsoft.com/wsl/feed/" Microsoft WSL)
  ;;         ("https://blogs.technet.microsoft.com/enterprisemobility/author/BradAnderson/feed/rss/" Microsoft)
  ;;         ("https://blogs.msdn.microsoft.com/bharry/feed" Microsoft)
  ;;         ("https://blogs.msdn.microsoft.com/powershell/feed/" Microsoft)
  ;;         ("https://weblogs.asp.net/scottgu/rss?containerid=13" Microsoft)
  ;;         ("https://blogs.msdn.microsoft.com/stevengu/feed/" Microsoft)
  ;;         ("https://code.visualstudio.com/feed.xml" Microsoft)
  ;;         ("http://blogs.msdn.microsoft.com/commandline/feed/" Microsoft)
  ;;         "https://www.paulekman.com/feed/"
  ;;         "https://github.blog/feed/"
  ;;         "https://blog.bitbucket.org/feed/"
  ;;         "https://www.blog.google/rss/"
  ;;         "https://calebmadrigal.com/atom.xml"
  ;;         )
  ;;       )
  (elfeed-org)
  )

(use-package elfeed-goodies
  ;;
  :disabled
  ;;
  :ensure t
  :defer t
  :config
  (elfeed-goodies/setup)
  )

(use-package elfeed-org
  ;;
  :disabled
  ;;
  :ensure t
  :defer t
  ;; :after (elfeed org)
  :config
  (elfeed-org)
  (setq rmh-elfeed-org-files (list "~/Dropbox/Sync/emacs/elfeed/feeds.org"))
  )

(use-package elfeed-protocol
  ;;
  :disabled
  ;;
  :ensure t
  :defer t
  )

(use-package pretty-mode
  :if (window-system)
  :ensure t
  :config
  ;; (global-pretty-mode t)
  )

(use-package csv-mode
  :ensure t
  :defer t
  )

;;--------------------------------------------------------
(use-package pdf-tools
  :ensure t
  :defer t
  :after (pdf-annot)
  :magic ("%PDF" . pdf-view-mode)
  ;; :bind (("h"   . 'pdf-annot-add-highlight-markup-annotation)
  ;;        ("t"   . 'pdf-annot-add-text-annotation)
  ;;        ("D"   . 'pdf-annot-delete)
  ;;        ("C-s" . 'isearch-forward)
  ;;        ("m"   . 'mrb/mailfile)
  ;;        ("<return>"   . 'pdf-annot-edit-contents-commit)
  ;;        ("<S-return>" .  'newline)
  ;;        ;; ("\\" . hydra-pdftools/body)
  ;;        ;; ("<s-spc>" .  pdf-view-scroll-down-or-next-page)
  ;;        ;; ("g"  . pdf-view-first-page)
  ;;        ;; ("G"  . pdf-view-last-page)
  ;;        ;; ("l"  . image-forward-hscroll)
  ;;        ;; ("h"  . image-backward-hscroll)
  ;;        ;; ("j"  . pdf-view-next-page)
  ;;        ;; ("k"  . pdf-view-previous-page)
  ;;        ;; ("e"  . pdf-view-goto-page)
  ;;        ;; ("u"  . pdf-view-revert-buffer)
  ;;        ;; ("al" . pdf-annot-list-annotations)
  ;;        ;; ("ad" . pdf-annot-delete)
  ;;        ;; ("aa" . pdf-annot-attachment-dired)
  ;;        ;; ("am" . pdf-annot-add-markup-annotation)
  ;;        ;; ("at" . pdf-annot-add-text-annotation)
  ;;        ;; ("y"  . pdf-view-kill-ring-save)
  ;;        ;; ("i"  . pdf-misc-display-metadata)
  ;;        ;; ("s"  . pdf-occur)
  ;;        ;; ("b"  . pdf-view-set-slice-from-bounding-box)
  ;;        ;; ("r"  . pdf-view-reset-slice)
  ;;        :map pdf-view-mode-map
  ;;        :map pdf-annot-edit-contents-minor-mode-map
  ;;        )
  :config
  ;; Some settings from http://pragmaticemacs.com/emacs/even-more-pdf-tools-tweaks/
  ;; (fullframe pdf-view-mode quit-window)
  (setq-default pdf-view-display-size 'fit-page) ;scale to fit page by default
  ;; (gsetq-default pdf-view-display-size 'fit-width)
  (setq pdf-annot-activate-created-annotations t ; automatically annotate highlights
        pdf-view-resize-factor 1.1		  ; more fine-grained zooming
        ;;pdf-misc-print-program "/usr/bin/lpr"
        pdf-view-midnight-colors '("#DCDCCC" . "#383838")) ; Not sure what this is
  ;; (add-hook 'pdf-view-mode-hook (lambda () (cua-mode 0))) ; turn off cua so copy works
  ;; (pdf-tools-install :no-query))						  ; no-query auto builds epfinfo when needed
  (pdf-tools-install)
  ;; (eval-after-load 'org '(require 'org-pdfview)
  )

;; ;;--------------------------------------------------------
;; (use-package pdf-tools
;;   :magic ("%PDF" . pdf-view-mode)
;;   :config
;;   (dolist
;;       (pkg
;;        '(pdf-annot pdf-cache pdf-dev pdf-history pdf-info pdf-isearch
;;                    pdf-links pdf-misc pdf-occur pdf-outline pdf-sync
;;                    pdf-util pdf-view pdf-virtual))
;;     (require pkg))
;;   (pdf-tools-install))

(use-package emms
  :ensure t
  :defer t
  :config
  (require 'emms-setup)
  (require 'emms-player-mplayer)
  (emms-all)
  (setq emms-player-list '(emms-player-mpg321
                           emms-player-ogg123
                           emms-player-mplayer))
  (defun emms-player-mplayer-volume(amount)
    (process-send-string
     emms-player-simple-process-name
     (format "volume %d\n" amount)))
  (setq emms-volume-change-function 'emms-player-mplayer-volume)
  (setq emms-source-file-default-directory "~/music/")
  (emms-add-directory-tree emms-source-file-default-directory)
  (emms-add-directory-tree "C:\Temp\_cancellami\_cancellami")
)

(use-package docker
  :ensure t
  :defer t
  )

(use-package dockerfile-mode
  :ensure t
  :defer t
  :mode ("/\\Dockerfile\\'")
  )

(use-package docker-compose-mode
  :ensure t
  :defer t
  )

(use-package simple-httpd
  :ensure t
  :defer t
  :config
  (setq httpd-port 7070)
  (setq httpd-host (system-name))
  )

(use-package impatient-mode
  :ensure t
  :defer t
  :after simple-httpd
  :commands impatient-mode
  )

(use-package markdown-mode
  :ensure t
  :defer t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown")
  :hook (markdown-mode-hook . gb/markdown-preview)
  :config 
  (progn
    (cond ((eq system-type 'windows-nt)
           ;; Windows
           (setq markdown-command "pandoc.exe -t html5")
           )
          ((eq system-type 'gnu/linux)
           ;; Linux
           (setq markdown-command "pandoc -t html5")
           ))
    (defun gb/markdown-filter (buffer)
      (princ
       (with-temp-buffer
         (let ((tmp (buffer-name)))
           (set-buffer buffer)
           (set-buffer (markdown tmp))
           (format "<!DOCTYPE html><html><title>Markdown preview</title><link rel=\"stylesheet\" href = \"https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/3.0.1/github-markdown.min.css\"/>
<body><article class=\"markdown-body\" style=\"box-sizing: border-box;min-width: 200px;max-width: 980px;margin: 0 auto;padding: 45px;\">%s</article></body></html>" (buffer-string))))
       (current-buffer)))
    (defun gb/markdown-preview ()
      "Preview markdown."
      (interactive)
      (unless (process-status "httpd")
        (httpd-start))
      (impatient-mode)
      (imp-set-user-filter 'gb/markdown-filter)
      (imp-visit-buffer))
    )
  )

(use-package auctex
  :ensure t
  :defer t
  )

(use-package company-auctex
  :ensure t
  :after (company auctex)
  :config
  (company-auctex-init)
  )

(use-package pcre2el
  :ensure t
  :delight
  :commands (rxt-mode rxt-global-mode)
  :config
  (pcre-mode)
  )

(use-package aggressive-indent
  :ensure t
  :defer t
  :diminish
  :hook (emacs-lisp-mode-hook . aggressive-indent-mode)
  )

(use-package highlight-indent-guides
  :ensure t
  :defer t
  :delight
  :hook (prog-mode-hook . highlight-indent-guides-mode)
  :custom 
  ((highlight-indent-guides-method 'character)
   (highlight-indent-guides-responsive 'stack))
  :config
  (unless (window-system)
    (set-face-background 'highlight-indent-guides-odd-face       "darkgray")
    (set-face-background 'highlight-indent-guides-even-face      "dimgray")
    (set-face-foreground 'highlight-indent-guides-character-face "dimgray"))
  )

(use-package flycheck
  :ensure t
  ;;:init (global-flycheck-mode)
  :defer t
  :hook (prog-mode-hook . flycheck-mode)
  )

(use-package flycheck-pos-tip
  :ensure t
  ;;:defines flycheck-pos-tip-timeout
  :hook (flycheck-mode-hook . flycheck-pos-tip-mode)
  :config (setq flycheck-pos-tip-timeout 30)
  )

(use-package flycheck-popup-tip
  :disabled
  :ensure t
  :defer t
  ;;:defines flycheck-pos-tip-timeout
  :hook (flycheck-mode-hook . flycheck-popup-tip-mode)
  ;; :config (setq flycheck-pos-tip-timeout 30)
  )

(use-package smartparens
  :ensure t
  :defer t
  :delight
  :hook (prog-mode-hook . smartparens-mode)
  :config
  (require 'smartparens-config)
  ;; (smartparens-global-mode)
  )

(use-package rainbow-delimiters
  :ensure t
  :defer t
  :hook (prog-mode-hook . rainbow-delimiters-mode)
  )

(use-package yasnippet
  :ensure t
  :defer t
  :hook (after-init-hook . yas-global-mode)
  ;; :init (yas-global-mode 1)
  :config (yas-reload-all)
  )

(use-package yasnippet-snippets
  :ensure t
  :defer t
  :after yasnippet
  )

(use-package projectile
  :ensure t
  :defer 5
  :config
  (setq projectile-completion-system 'ivy)
  ;; (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode 1)
  )

(use-package ibuffer-projectile
  :ensure t
  :defer t
  :after (projectile)
  )

(use-package counsel-projectile
  :ensure t
  :defer t
  :after (counsel projectile)
  :hook (projectile-mode-hook . counsel-projectile-mode)
  )

(use-package treemacs-projectile
  :ensure t
  :defer t
  :after treemacs projectile
  :hook (treemacs-mode-hook . treemacs-project-mode)
  )

(use-package powershell
  :ensure t
  :defer t
  )

(use-package csharp-mode
  :ensure t
  :defer t
  )

(use-package sql-indent
  :ensure t
  :defer t
  )

(use-package go-mode
  :ensure t
  :defer t
  )

(use-package go-errcheck
  :ensure t
  :defer t
  :after go-mode
  )

(use-package company-go
  :ensure t
  :after (company go-mode)
  )

(defun gb/rust/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm
  ;; (setq-local buffer-save-without-query t)
  (cargo-minor-mode t)
  (company-mode t)
  )

(use-package rustic
  :ensure t
  :defer t
  ;; :bind (:map rustic-mode-map
  ;;             ("M-j" . lsp-ui-imenu)
  ;;             ("M-?" . lsp-find-references)
  ;;             ("C-c C-c l" . flycheck-list-errors)
  ;;             ("C-c C-c a" . lsp-execute-code-action)
  ;;             ("C-c C-c r" . lsp-rename)
  ;;             ("C-c C-c q" . lsp-workspace-restart)
  ;;             ("C-c C-c Q" . lsp-workspace-shutdown)
  ;;             ("C-c C-c s" . lsp-rust-analyzer-status))
  :hook ((rustic-mode-hook . gb/rust/rustic-mode-hook))
  :config
  ;; uncomment for less flashiness
  ;; (setq lsp-eldoc-hook nil)
  ;; (setq lsp-enable-symbol-highlighting nil)
  ;; (setq lsp-signature-auto-activate nil)

  ;; comment to disable rustfmt on save
  ;; (setq rustic-format-on-save t)

  ;; (setq rustic-lsp-server 'rls)
  ;; attenzione, il .exe va bene solo su windows
  (setq lsp-rust-analyzer-server-command '("~/.cargo/bin/rust-analyzer.exe"))
  (setq rustic-lsp-client 'eglot)
  (push 'rustic-clippy flycheck-checkers)
  (setq rustic-flycheck-clippy-params "--message-format=json")
  )

(use-package flycheck-rust
  :ensure t
  :defer t
  :after (flycheck)
  :hook (flyckeck-mode-hook . flycheck-rust-setup)
  ;; :hook ((rust-mode-hook . flycheck-rust-setup)
  ;;        (flycheck-mode . flycheck-rust-setup)
  ;;        )
  ;; :after (flycheck rust-mode)
  ;; :config (flycheck-rust-setup)
  )

(use-package rust-mode
  ;; ---------------------
  :disabled
  ;; ---------------------
  :ensure t
  :defer t
  :after (company flycheck-rust cargo)
  :hook ((rust-mode-hook . company-mode)
         ;; (rust-mode-hook . flycheck-rust-setup)
         (rust-mode-hook . cargo-minor-mode)
         )
  :config
  (setq indent-tabs-mode nil)
  ;; (setq rust-format-on-save t)
  (setq company-tooltip-align-annotations t)
  )

(use-package cargo
  :ensure t
  :defer t
  ;; :after rust-mode
  ;; :after rustic-mode
  ;; :hook ((rust-mode-hook . cargo-minor-mode)
  ;;        ;; (conf-toml-mode-hook . cargo-minor-mode)
  ;;        )
  )

(use-package lsp-mode
  ;; ------------------------------
  :disabled
  ;; ------------------------------
  :ensure t
  :commands (lsp)
  :after (yasnippet)
  ;; Ricordarsi di leggere la documentazione
  ;; sul sito del produttore.
  :custom
  (lsp-keymap-prefix "C-c l")
  :hook (;; Avvio normale (XXX-mode . lsp)
         ;; Avvio differito (XXX-mode   . lsp-deferred)
         (c++-mode-hook        . lsp-deferred)
         (c-mode-hook          . lsp-deferred)
         (csharp-mode-hook     . lsp-deferred)
         (css-mode-hook        . lsp-deferred)
         (dart-mode-hook       . lsp-deferred)
         (go-mode-hook         . lsp-deferred)
         (groovy-mode-hook     . lsp-deferred)
         (haxe-mode-hook       . lsp-deferred)
         (html-mode-hook       . lsp-deferred)
         (java-mode-hook       . lsp-deferred)
         (js-mode-hook         . lsp-deferred)
         (json-mode-hook       . lsp-deferred)
         (kotlin-mode-hook     . lsp-deferred)
         (latex-mode-hook      . lsp-deferred)
         (less-css-mode-hook   . lsp-deferred)
         (nxml-mode-hook       . lsp-deferred)
         (powershell-mode-hook . lsp-deferred)
         (python-mode-hook     . lsp-deferred)
         (rust-mode-hook       . lsp-deferred)
         (scss-mode-hook       . lsp-deferred)
         (sh-mode-hook         . lsp-deferred)
         ;; (sql-mode-hook        . lsp-deferred)
         (typescript-mode-hook . lsp-deferred)
         (xml-mode-hook        . lsp-deferred)
         (yaml-mode-hook       . lsp-deferred)
         (clojure-mode-hook    . lsp-deferred)

         (lsp-mode-hook        . lsp-enable-which-key-integration)
         )
  :commands (lsp lsp-deferred lsp-doctor)
  :config
  ;; (setq lsp-completion-enable-additional-text-edit nil)
  ;; Come riportato qui: https://emacs-lsp.github.io/lsp-mode/page/performance/
  (setq lsp-completion-provider :capf)
  ;; (setq lsp-log-io t
  ;;       lsp-server-trace "verbose")
  )

(use-package company-lsp
  ;; ------------------------------
  :disabled
  ;; ------------------------------
  :ensure t
  :disabled
  :after (lsp-mode company)
  :config
  (setq company-lsp-enable-snippet t)
  (push 'company-lsp company-backends)
  )

(use-package lsp-ui
  ;; ------------------------------
  :disabled
  ;; ------------------------------
  :ensure t
  ;; :defer t
  :after (lsp-mode markdown-mode)
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil)
  )

(use-package lsp-ivy
  ;; ------------------------------
  :disabled
  ;; ------------------------------
  :ensure t
  ;; :defer t
  :after (lsp-mode ivy)
  :commands lsp-ivy-workspace-symbol
  )

(use-package dap-mode
  ;; ------------------------------
  :disabled
  ;; ------------------------------
  :ensure t
  :defer t
  :after (lsp-mode lsp-treemacs)
  )
;; (use-package dap-csharp
;;   :ensure t
;;   :defer t
;;   )

(use-package lsp-treemacs
  ;; ------------------------------
  :disabled
  ;; ------------------------------
  :ensure t
  :after (lsp-mode treemacs)
  :commands (lsp-treemacs-errors-list)
  :custom 
  (lsp-treemacs-sync-mode 1)
  )

(use-package lsp-origami
  ;; ------------------------------
  :disabled
  ;; ------------------------------
  :ensure t
  :defer t
  :after (lsp-mode origami)
  :hook (lsp-after-open-hook . lsp-origami-try-enable)
  )

;; (use-package lsp-mssql
;;   :ensure t
;;   :defer t
;;   :after (lsp-mode lsp-treemacs)
;;   :hook (sql-mode-hook . lsp)
;;   )

(use-package eglot
  :ensure t
  :defer t
  ;; :custom
  ;; (eglot-autoreconnect nil)
  ;; (eglot-autoshutdown t)
  :hook ((c++-mode-hook        . eglot-ensure)
         (c-mode-hook          . eglot-ensure)
         (csharp-mode-hook     . eglot-ensure)
         (css-mode-hook        . eglot-ensure)
         (dart-mode-hook       . eglot-ensure)
         (go-mode-hook         . eglot-ensure)
         (groovy-mode-hook     . eglot-ensure)
         (haxe-mode-hook       . eglot-ensure)
         (html-mode-hook       . eglot-ensure)
         (java-mode-hook       . eglot-ensure)
         (js-mode-hook         . eglot-ensure)
         (json-mode-hook       . eglot-ensure)
         (kotlin-mode-hook     . eglot-ensure)
         (latex-mode-hook      . eglot-ensure)
         (less-css-mode-hook   . eglot-ensure)
         (nxml-mode-hook       . eglot-ensure)
         (powershell-mode-hook . eglot-ensure)
         (python-mode-hook     . eglot-ensure)
         (rust-mode-hook       . eglot-ensure)
         (scss-mode-hook       . eglot-ensure)
         (sh-mode-hook         . eglot-ensure)
         (sql-mode-hook        . eglot-ensure)
         (typescript-mode-hook . eglot-ensure)
         (xml-mode-hook        . eglot-ensure)
         (yaml-mode-hook       . eglot-ensure)
         (clojure-mode-hook    . eglot-ensure))
  )

;; ===========================================================================
;; Local Variables:
;; coding: utf-8-unix
;; indent-tabs-mode: nil
;; tab-width: 4
;; End:
;; ===========================================================================

;;; gb-init ends here
