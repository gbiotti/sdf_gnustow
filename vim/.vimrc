set nocompatible

if has("gui_running")
   let do_syntax_sel_menu=1
endif

set langmenu=none
if has("win32") || has("win64")
   language messages en_US
else
    language messages en_US.UTF-8
endif

if has("multi_byte")
    if has("win32") || has("win64")
        if has("gui_running")
            set encoding=utf-8
        endif
    endif
    set encoding=utf-8
endif

set nocompatible

" set modeline

set backspace=indent,eol,start

" Highlight problematic whitespace
" set listchars=tab:>.,trail:.,extends:#,nbsp:.
" set listchars=eol:�,tab:�,trail:�,extends:>,precedes:<,nbsp:�
" il carattere per eol      (�) si ottiene con CTRL-vu00b6
" il carattere per tab      (�) si ottiene con CTRL-vu00bb
"                               seguito da \<spazio> oppure
" il carattere per trail    (�) si ottiene con CTRL-vu00b7
" il carattere per extends  (>) e' il carattere di maggiore
" il carattere per precedes (<) e' il carattere di minore
" il carattere per nbsp     (�) si ottiene con CTRL-vu00a4
set listchars=eol:�,tab:�\ ,trail:�,extends:>,precedes:<,nbsp:�

set number
set relativenumber
set history=50
set incsearch
set ignorecase
set smartcase
set wrapscan

" Make the 'cw' and like commands put a $ at the end instead of
" just deleting the text and replacing it
set cpoptions=ces$

set statusline=%<%F\ %h%m%r%w%q\ %y\(%{&ff}\)\ %=\ \#%n\ ln:%l\/%L[%P]\ co:%c%V\ %b

set lazyredraw
set showmode
set foldenable
set foldopen=block,insert,jump,mark,percent,quickfix,search,tag,undo
set whichwrap=b,s,h,l,<,>,[,]
set scrolljump=0
set scrolloff=0
set sidescrolloff=0
set wildmenu
set showfulltag
set diffopt+=iwhite
set clipboard+=unnamed
set grepprg=grep\ -nH\ $*

" let loaded_matchparen=1

set showtabline=2
set nostartofline
set nospell	" spell checking off (default!)
if has("autocmd")
    filetype plugin indent on
    augroup vimrcEx
        au!
        autocmd BufReadPost *
            \ if line("'\"") > 1 && line("'\"") <= line("$") |
            \     exe "normal! g`\"" |
            \ endif
    augroup END

endif " has("autocmd")

set nowrap
set autoindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
set noexpandtab

if has("mouse")
    set mouse=a
endif

if &t_Co > 2 || has("gui_running")
    syntax enable
    set hlsearch
    set synmaxcol=2048
endif

if has("cmdline_info")
    set noruler
    set showcmd
endif

if has("statusline")
    set laststatus=2
    set statusline=%<%f\                     " Filename
    set statusline+=%w%h%m%r                 " Options
    set statusline+=\ [%{&ff}/%Y]            " filetype
    set statusline+=\ [%{getcwd()}]          " current dir
    "set statusline+=\ [A=\%03.3b/H=\%02.2B] " ASCII / Hexadecimal value of char
    set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
endif

if has("gui_running")
    " GUI
    set cursorline
    set guicursor=n-v-c:block-Cursor-blinkon0,ve:ver35-Cursor,o:hor50-Cursor,i-ci:ver25-Cursor,r-cr:hor20-Cursor,sm:block-Cursor-blinkwait175-blinkoff150-blinkon175
    set cmdheight=2       " Abbreviato: set ch=2
    set mousehide
endif

set shortmess+=I

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

set background=dark

"
" vim: set tabstop=4:shiftwidth=4:filetype=vim:fdm=marker:fileformat=unix:
"
